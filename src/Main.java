// Include the Dropbox SDK.
import com.dropbox.core.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Locale;

import javax.annotation.processing.FilerException;

public class Main {
	final static String APP_KEY = "ehzmj210npq7zed";
	final static String APP_SECRET = "5exbng9brfszs0c";
	final static String MEGA_PATH = "/root/mega/dropbox";
	static String AUTH = "";
	static String STARTING_PATH = "/";
	static DbxClient client;
	static Long size = new Long(0);

	public static void main(String[] args) throws IOException, DbxException {
		// Get your app key and secret from the Dropbox developers website.
		if (args.length > 0) {
			AUTH = args[0];
			if (args.length > 1) {
				STARTING_PATH = args[1];
			}
			System.out.println("Is Running");
			Main m = new Main();
			m.run();
		} else {
			System.out
					.println("Usage: java -jar dtm.jar AUTH_LONG_CODE [STARTING_PATH]");
			System.exit(0);
		}
	}

	private void run() throws IOException, DbxException {
		DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);

		DbxRequestConfig config = new DbxRequestConfig("JavaTutorial/1.0",
				Locale.getDefault().toString());
		DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);

		// Have the user sign in and authorize your app.
		webAuth.start();
		/*
		 * System.out.println("1. Go to: " + authorizeUrl); System.out
		 * .println("2. Click \"Allow\" (you might have to log in first)");
		 * System.out.println("3. Copy the authorization code."); String code =
		 * new BufferedReader(new InputStreamReader(System.in))
		 * .readLine().trim();
		 */

		// This will fail if the user enters an invalid authorization code.
		DbxAuthFinish authFinish = webAuth.finish(AUTH);
		String accessToken = authFinish.accessToken;

		client = new DbxClient(config, accessToken);

		System.out.println("Linked account: "
				+ client.getAccountInfo().displayName);

		long startTime = System.nanoTime();
		readFolder(STARTING_PATH);
		long endTime = System.nanoTime();
		double secs = (double) (((double) (endTime - startTime)) / Math.pow(10,
				9));
		System.out.println("took " + secs + " seconds");
		System.out.println("Final size: "+size +" bytes");
	}

	public void readFolder(String path) throws DbxException, IOException {
		DbxEntry.WithChildren listing = client.getMetadataWithChildren(path);
		for (DbxEntry child : listing.children) {
			String uniformPath = child.path.toLowerCase();
			if (child.isFolder()) {
				readFolder(child.path.toLowerCase());
			}
			if (child.isFile()) {
				String uniformName = child.name.toLowerCase();
				size += child.asFile().numBytes;
				if (!isFileSynced(uniformPath)) {
					downloadFile(child.path, uniformName);
					moveToMega(uniformPath, uniformName);
				} else {
					System.out.println("Escaped file " + uniformPath);
				}
			}
		}
	}

	public boolean isFileSynced(String path) {
		try (BufferedReader br = new BufferedReader(new FileReader("log.txt"))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains(path))
					return true;
				else
					continue;
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void newFileSynced(String path) {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter("log.txt", true)))) {
			out.println(path);
			out.close();
		} catch (IOException e) {
			// exception handling left as an exercise for the reader
		}

	}

	public void moveToMega(String path, String file) {
		String onlyPath = MEGA_PATH
				+ path.substring(0, path.length() - 1 - file.length());
		File theFile = new File(onlyPath);
		theFile.mkdirs();
		file = file.replaceAll("[^a-zA-Z0-9.-]", "_");

		onlyPath = onlyPath.replaceAll(" ", "\\ ");
		// System.out.println("path: " + path + "\nonlypath: " + onlyPath);
		try {
			Files.move(Paths.get(file), Paths.get(onlyPath + "/" + file));
			newFileSynced(path);
			System.out.println("added file: " + path);
		} catch (java.nio.file.FileAlreadyExistsException e) {
			newFileSynced(path);
			System.out.println("file exists: " + path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clearCache();
		File n1 = new File("./" + file);
		n1.delete();
	}

	public void clearCache() {
		try {
			String[] b = new String[] {"/bin/sh", "-c", "rm -r /tmp/mega*"};
			Runtime.getRuntime().exec(b);
			System.out.println("Cache cleared (hopefully)");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public void downloadFile(String path, String file) {

		// file = file.replaceAll(" ", "\\ ");
		file = file.replaceAll("[^a-zA-Z0-9.-]", "_");
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream("./" + file);
			@SuppressWarnings("unused")
			DbxEntry.File downloadedFile = client.getFile(path, null,
					outputStream);
		} catch (DbxException | IOException e) {
			// TODO Auto-generated catch block
			System.out.println("File: " + path);
			e.printStackTrace();
		} finally {
			try {
				outputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}